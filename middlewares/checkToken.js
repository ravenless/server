import jwt from 'jsonwebtoken'

import config from '../config'

export default async (req, res, next) => {
    const token = req.headers['token']
    if (!token) {
        return next({
            status: 403,
            message: 'Неправильные параметры запроса'
        })
    }
    let tokenObj
    try {
        tokenObj = jwt.verify(token, config.secret)
    } catch ({ message }) {
        return next({
            status: 400,
            message: 'Ошибка идентификации'
        })
    }

    req.token = token
    next()
}