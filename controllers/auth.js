import jwt from 'jsonwebtoken'

import User from '../models/user'
import config from '../config'

export const signup = async (req, res, next) => {
    const credentials = req.body
    let user
    try {
        user = await User.create(credentials)
    } catch({ message }) {
        return next({
            status: 400, 
            message: 'Пользователь с таким логином или email уже существует!'
        })
    }

    res.json(user)
}

export const signin = async (req, res, next) => {
    const { login, password } = req.body

    const user = await User.findOne({ login })

    if (!user) {
        return next({
            status: 400,
            message: 'Пользователь не найден'
        })
    }

    try {
        const result = user.comparePasswords(password)
    } catch(e) {

        return next({
            status: 401,
            message: 'Пароль не верный'
        })
    }

    const token = jwt.sign({ _id: user._id}, config.secret)
    res.json(token)  
}

export const verifyLogin = async (req, res, next) => {
    const { login } = req.body
    
    const user = await User.findOne({ login })

    res.json( !!user )
}

export const verifyEmail = async (req, res, next) => {
    const { email } = req.body

    const user = await User.findOne({ email })

    res.json( !!user )
}