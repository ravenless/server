import mongoose, { Schema } from 'mongoose'
import bcrypt from 'bcrypt-as-promised'

const UserSchema = new Schema({
    name: String,
    login: { type: String, unique: true, index: true },
    password: String,
    email: { type: String, unique: true, index: true }
})

UserSchema.pre('save', async function (next) {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.password, salt)

    this.password = hash
    next()
})

UserSchema.methods.comparePasswords = function(password) {
    return bcrypt.compare(password, this.password)
}

export default mongoose.model('user', UserSchema)